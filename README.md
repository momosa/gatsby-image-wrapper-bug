Demonstrates a bug with `gatsby-image` v2.4.13 when used with `wrapPageElement`.
The `gatsby-image` `Img` component in the wrapper isn't cached,
but the regular image is.

1. Go to
[https://momosa.gitlab.io/gatsby-image-wrapper-bug](https://momosa.gitlab.io/gatsby-image-wrapper-bug)
or run locally (use `gatsby serve`, not `gatsby develop` to see the effect)
1. In your dev tools, enable cache and throttle network to something very slow.
1. Switch pages (there are links provided on the page to make switching easy).
1. Check the console log to see see when the gatsby image mounts and unmounts
(this appears to be an issue with the image cache, not unmounting).
