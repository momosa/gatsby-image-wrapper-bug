import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

class SampleGatsbyImage extends React.Component {

  componentDidMount() {
    console.log("Gatsby image mounted")
  }

  componentWillUnmount() {
    console.log("Gatsby image will unmount")
  }

  render() {
    return (
      <StaticQuery
        query={imageQuery}
        render={data => <Img fixed={data.file.childImageSharp.fixed} />}
      />
    )
  }
}

const imageQuery = graphql`
  query {
    file(relativePath: { eq: "images/example.jpg" }) {
      childImageSharp {
        fixed(width: 300, height: 300) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`


export default SampleGatsbyImage
