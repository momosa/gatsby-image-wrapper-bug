import React from "react"
import { Link } from "gatsby"
import SampleGatsbyImage from "./sample-gatsby-image"
import exampleImage from "../images/example.jpg"

class Layout extends React.Component {

  render() {
    const headerStyle = {
      display: "grid",
      gridTemplateColumns: "1fr 1fr",
    }

    return (<>
      <h1>Layout wrapper</h1>

      <p><Link to="/">Go to index page</Link></p>
      <p><Link to="/other">Go to other page</Link></p>

      <div style={headerStyle}>
        <div>
          <SampleGatsbyImage />
          <p>Gatsby image fetched with StaticQuery</p>
        </div>

        <div>
          <img src={exampleImage} alt="Taters" />
          <p>Regular image</p>
        </div>
      </div>

      <hr />

      <h2>Page content</h2>

      {this.props.children}

    </>)
  }
}

export default Layout
